import React,{ Component } from 'react';
import './App.css';
import Axios from 'axios';
import TopNav from './components/TopNav';
import Home from './components/Home';
import Footer from './components/Footer';
import DealerLocator from './components/DealerLocator'
import TestFlightForm from './components/TestFlightForm';
import {BrowserRouter as Router,Route} from 'react-router-dom';
import VehicleDetail from './components/VehicleDetail';
import BuildAndPrice from './components/BuildAndPrice';

class App extends Component {
  constructor(props){
    super(props);
    //when app creaet vehicleData is null
    this.state={vehicleData: null};
  }
  //Now, refere to the life cycle, we should have ComponentDidMount to initialize data.
  componentDidMount(){
    //use axios to get data from db
    Axios.get("  http://localhost:3001/vehicles")
    //then functions after get data, res is response from get
    .then (res => {
      console.log(res.data);
      this.setState({vehicleData: res.data});
    })
    //catch function use for when error occures
    .catch (err => console.log(err))
  }
  render(){
  if (this.state.vehicleData){
  return (
    <Router>
      <div className="App">
        {/* Now we pass vehicleData to TopNav */}
        {/* Every Expression in react is in {} , and now our TopNav comp has property with Name vehicleData */}
        <TopNav vehicleData={this.state.vehicleData} />
        {/* Each component that will be change while changin pages in browser will be here */}
          <div className="contentArea">
            {/* if we want define component only: <Route exact path="/" component={Home} />  */}
            {/* if we want define component and pass property  */}
             <Route exact path="/" render={(props) => <Home {...props} vehicleData={this.state.vehicleData} />} />
             <Route path="/find-a-dealer" component={DealerLocator} />
             <Route path="/build-and-price" render={(props) => <BuildAndPrice {...props} vehicleData={this.state.vehicleData} />} />
             <Route path="/schedule-test-flight" component={TestFlightForm} />
             <Route path="/detail/:selectedVehicle" render={(props) => <VehicleDetail {...props} vehicleData={this.state.vehicleData} />} /> 
          </div>
        <Footer />
      </div>
      </Router>
  );}
  //if there was no data it will be return null
  else{
    return(null);
  }
}
}

export default App;
