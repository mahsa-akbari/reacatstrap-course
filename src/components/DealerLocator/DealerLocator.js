import React from 'react';
import Axios from 'axios';
import {
    Row,Col,Form,FormGroup,Input, InputGroup,ListGroupItem,Badge, Table,
    Button,ListGroup,InputGroupAddon
} from 'reactstrap'
import './DealerLocator.css';

class DealerLocator extends React.Component{
    constructor(props){
        super(props);
        this.state={searchTerm:"", dealerships:null};
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onClearClicked = this.onClearClicked.bind(this);
        this.onListClick = this.onListClick.bind(this);
    }
    handleInputChange(eventData){
        // this is a js code to get value from input.
        this.setState({searchTerm:eventData.target.value});
    }
    onClearClicked(eventData){
        eventData.preventDefault();
        this.setState({searchTerm:''});

    }
    onListClick(eventData){
        eventData.preventDefault();
        const stateClicked = eventData.target.text.split(" ")[0];
        this.setState({searchTerm:stateClicked});
    }
    
    componentDidMount(){
        Axios.get("http://localhost:3001/dealerships").then(
            res => {
                // stateCounter is a counter to counte number of dealers in each state. reduce is a js function.
                let stateCounter = res.data.reduce(
                    function(dealerStateCount,dealer){
                        dealerStateCount[dealer.state] =  (dealerStateCount[dealer.state] || 0) + 1;
                        return dealerStateCount
                        }
                    ,this
                );
                // stateCounter  is not defined in cinstructor, but it doesn't matter. it will be create in here.
                this.setState({dealerships:res.data, stateCounter:stateCounter})
            }
        ).catch(err => console.log(err));
    }

    render(){
        if(this.state.dealerships){
            const filteredStubData = this.state.dealerships.filter(
                d => d.state.includes(this.state.searchTerm)
            );
            let searchBar = <div className="rtl">
                <Row>
                <Col sm={{size:8, offset:3}} md={{size:6, offset:3}}>
                <h1>میان{this.state.dealerships.length} فروشنده مجاز در سراسر کشور</h1>
                </Col>
                </Row>
                <Row>
                    {/* this md attribute help to searchbar be in the middle of page.  */}
                    <Col sm={12} md={{size:6, offset:3}}>
                        <Form>
                            <FormGroup>
                                <InputGroup>
                                {/* name attribute is using for backend. */}
                                <Input type="text" onChange={this.handleInputChange}
                                value={this.state.searchTerm}
                                name="user_address"
                                placeholder="ما نزدیک شما هستیم. شما در چه استانی هستید؟"
                                />
                                <InputGroupAddon addonType="append">
                                    <Button onChange={this.onClearClicked}>
                                        X
                                    </Button>
                                </InputGroupAddon>
                                </InputGroup>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </div>
            if (this.state.searchTerm.length <1){
                let stateCounterMarkup = null;
                if (this.state.stateCounter){
                    stateCounterMarkup =
                    <Row className="rtl">
                        <Col sm={12} md={{size:10,offset:1}}>
                            <ListGroup>
                                    {Object.keys(this.state.stateCounter).sort().map(
                                        function(key,i){
                                            // key is "state" and  "stateCounter[key]" is number of dealers
                                            if (typeof this.state.stateCounter[key] === 'number'){
                                                return (<ListGroupItem tag="a" href="#" key={key + 1} 
                                                onClick={this.onListClick} className="justify-content-between">
                                                    {key} <Badge pill>{this.state.stateCounter[key]}</Badge>
                                                </ListGroupItem>);
                                            } else{
                                                return null
                                            }
                                        },this)}
                            </ListGroup>

                        </Col>
                    </Row>
                }
                return (<div>
                    {searchBar}
                    {stateCounterMarkup}
                </div>);
                
            }
            else{
            return (<div>{searchBar}

            <Row className="rtl">
                <Col sm={12} md={{size:10, offset:1}}>
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>فروشندگان</th>
                                <th>آدرس</th>
                                <th>شهر</th>
                                <th>استان</th>
                                <th>کد پستی</th>
                                <th>تلفن</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredStubData.map((item,i) =>{
                                return(<tr key={item.phone}>
                                    <td>{String(i)}</td>
                                    <td>{item.dealershipName}</td>
                                    <td>{item.address}</td>
                                    <td>{item.city}</td>
                                    <td>{item.state}</td>
                                    <td>{item.zip}</td>
                                    <td>{item.phone}</td>
                                </tr>);
                            })}
                        </tbody>

                    </Table>
                </Col>
            </Row>
            </div>    
                );
        }}
        else{
            return null
        }
    }
}
export default  DealerLocator; 