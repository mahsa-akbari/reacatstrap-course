import React from 'react';
import './TopNav.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
  } from 'reactstrap';
import { Link } from 'react-router-dom';

// Now create class, name of class should be same as Folder
class TopNav extends React.Component{
    // We create empty constructor now
    constructor(props){
        super(props);
        //bind toggle to toggle method!
        this.toggle = this.toggle.bind(this)
        this.state = {
            isOpen : false
        };
    }
    //toggle method to change isOpen
    toggle(){
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render(){
        // Now we are gonna use vehicleData property in here, map is function
        //each item in table is item and we can access to attributes like this: item.model
        //map is given two input, first arrowFunc and secound is this.
        const vehicleSelection = this.props.vehicleData.map((item)=>{
          return(
          <DropdownItem key={item.detailkey}>
              <Link to={{pathname:"/detail/" + item.detailKey}}>
                {item.model}
              </Link>
          </DropdownItem>
        )},this); 
      
        return (
          //add rtl class, because our lang is persian
                <div className="rtl">
                  <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">نمایندگی اتومبیل پرنده</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                      <Nav className="mr-auto" navbar>
                        <NavItem>
                          <NavLink href="/"><i className="fas fa-home"></i> خانه</NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                          <DropdownToggle nav caret>
                          <i className="fas fa-space-shuttle"></i> اتوموبیل های پرنده
                          </DropdownToggle>
                          <DropdownMenu right>
                            {/* We use Data-Driven DropDown */}
                            {vehicleSelection}
                          </DropdownMenu>
                        </UncontrolledDropdown>
                        <NavItem>
                          <NavLink href='/find-a-dealer'><i className="fas fa-map-marker-alt"></i> پیدا کردن فروشنده</NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink href='/build-and-price'><i className="fas fa-cog"></i> بسازید و قیمت گذاری کنید</NavLink>
                        </NavItem>
                      </Nav>
                      
                      
                    </Collapse>
                  </Navbar>
                </div>
        );
    }
}
// in the last line should have export,nevertheles the component would not show up.
export default TopNav;