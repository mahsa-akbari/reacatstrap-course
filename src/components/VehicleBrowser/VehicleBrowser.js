import React from 'react';
import './VehicleBrowser.css';
import { Container, Row, Col, Card, CardImg, CardText,CardBody,CardTitle,
    CardSubtitle ,NavLink} from 'reactstrap';
import Numeral from 'numeral';
class VehicleBrowser extends React.Component{
    
    render(){
        const cards = this.props.vehicleData.map((item) =>{
            return(
                        // Key is important to set, it should be unique in whole site so we add "vb" end of it!
                        //do not hard code md, so we will calculate this!
                        <Col key={item.detailKey + "vb"} md={Math.ceil(12/this.props.vehicleData.length)}>
                            <Card>
                                <CardImg top width="100%" src={item.thumbnail}/>
                                <CardBody>
                                <CardTitle tag="h5">{item.year} {item.model}</CardTitle>
                                <CardSubtitle>{item.tagline}</CardSubtitle>
                                <CardText>Start at {Numeral(item.msrp).format('$0,0')}</CardText>
                                </CardBody>
                                <NavLink href={"/detail/" + item.detailKey}>Details</NavLink>
                                <NavLink href="/build-and-price">Build & Price</NavLink>
                                <NavLink href="/find-a-dealer">Dealers Near you</NavLink>
                            </Card>
                        </Col>
            )
        });
        return (
            <div className="VehicleBrowser">
                <Container>
                    <Row>
                      {cards} 
                    </Row>
                </Container>
            </div>
        );
    }
}
export default  VehicleBrowser; 