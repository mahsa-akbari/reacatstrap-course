import React from 'react';
import './TestFlightForm.css';
import Axios from 'axios';
import {Alert,InputGroup,InputGroupAddon,
Card,CardText,CardBody,CardTitle,CardSubtitle,Button,Form,FormGroup,Input} from 'reactstrap';

class TestFlightForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {showSuccess:false,showDanger:false};
        this.handleInputChange= this.handleInputChange.bind(this);
        this.onSubmit= this.onSubmit.bind(this)
    }
    // target is the tag we use this method for OnClick;
    handleInputChange(eventData){
        const target = eventData.target;
        const value = target.type === 'checkbox' ? target.checked:target.value;
        const name = target.name;
        this.setState({[name] : value})
    }
    onSubmit(eventData){
        eventData.preventDefault();
        Axios.post(' http://localhost:3001/mailingList',{
            // All of this created when handleInputChange call.
            customerName: this.state.customerName,
            email:this.state.customerEmail,
            phone:this.state.customerPhone,
            budget:this.state.Budget

        }).then(
            (res)=>{
                this.setState({showSuccess:true,showDanger:false});
            }
        ).catch(
            (err) => {
                this.setState({showSuccess:false,showDanger:true});
            }
        );

    }
    render(){
        
        return (<div className="rtl">
            
            <Card>
                <CardBody>
                    <CardTitle className="TestFlightForm"><h2>
                        زمان بندی پرواز تستی
                        </h2>
                    </CardTitle>
                    <CardSubtitle className="TestFlightForm"><h4>
                        نیازی به گواهی پرواز نیست!
                        </h4>
                    </CardSubtitle>
                    <CardText className="TestFlightForm">
                        فرم زیر را برای زمان بندی پرواز تستی پر کنید.
                    </CardText>
                    <Form>
                        <FormGroup>
                            <Input type="text" name="customerName" id="customerName" placeholder="اسم شما چیست؟" onChange={this.handleInputChange}></Input>
                        </FormGroup>
                        <br />
                        <FormGroup>
                            <Input type="text" name="customerPhone" id="customerPhone" placeholder="شماره تلفن شما چیست؟" onChange={this.handleInputChange}></Input>
                        </FormGroup>
                        <br />
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                        <Input type="text" name ="customerEmail" id="customerEmail" placeholder="ایمیل شما چیست؟" onChange={this.handleInputChange}></Input>
                        </InputGroup>
                        <br />
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                        <Input type="text" name ="Budget" id="Budget" placeholder="آیا بودجه ای دارید ؟" onChange={this.handleInputChange}></Input>
                        </InputGroup>
                        <br />
                    </Form>
                    <br />
                    <div className="TestFlightForm"><Button onClick={this.onSubmit}>ارسال</Button></div>
                    <Alert color="success TestFlightForm" isOpen={this.state.showSuccess}>
                        داده های شما با موفقیت ارسال شدند!
                    </Alert>
                    <Alert color="danger TestFlightForm" isOpen={this.state.showDanger}>
            یک چیزی در فرم شما اشتباه شده است!
         </Alert>
                </CardBody>
            </Card>
            
        </div>
            );
    }
}
export default  TestFlightForm; 